// This program tests to make sure that null fields returned from methods are checked and that
// the static analyzer handles constantly overriding the variable name that is being used to check
// for null pointer.
class ThirdTryIsNotTheCharm {
  public static void main(String[] a){
      System.out.println(new Run().run());
  }
}

class Run {
  public int run(){
    int ret;
    A whatAmI;

    whatAmI = new A();
    ret = whatAmI.setField();
    // First one returns the original's field.
    whatAmI = whatAmI.unsafeA();
    // Second one returns the field's field, which is null.
    whatAmI = whatAmI.unsafeA();
    // Third one is null pointer.
    whatAmI = whatAmI.unsafeA();

    return 0;
  }
}

class A {
  A a;
  public int setField() {
    a = new A();
    return 0;
  }

  public A unsafeA() {
    return a;
  }
}
